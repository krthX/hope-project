<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>HOPE</title>
	<link href="bootstrap.css" rel="stylesheet">    
    <link href="bootstrap-responsive.css" rel="stylesheet">
	<link href="css/style_login.css" rel="stylesheet">
	<link rel="stylesheet" href="css/bootstrap.min.css">    
  </head>

  <body>
 	<nav class="navbar navbar-inverse">
			<div class="container-fluid">
				<div class="navbar-header">
					<a class="navbar-brand" href="#">HOPE</a>
				</div>
		
				<div class ="pull-right">
					<ul class="nav navbar-nav" >
						
						<li><a href="Login.jsp">Login</a></li> 					 
						<li><a href="#">Registrate</a></li>
						
												
					</ul>
					
					
				</div>
				
				<div class ="pull-left">
					<ul class="nav navbar-nav" >
						<li><a href="Inicio"></a></li> 					 
						<li><a href="">Nosotros</a></li>
						<li><a href="Habitaciones.jsp">Habitaciones</a></li>
					</ul>
				</div>
			</div>
		</nav>
  

    <div class="container">

      <form class="form-signin" action="login.jsp" method="POST">
        <h2 class="form-signin-heading">Iniciar Sesi�n</h2>
        <input type="text" class="input-block-level" name="email" placeholder="@ correo electronico">
        <input type="password" class="input-block-level" name="password" placeholder="contrase�a">
        <label class="checkbox">
        	<div class="pull-right">
          <input type="checkbox" value="remember-me"> Recordar
          </div>
        </label>
        <button class="btn btn-large btn-primary" type="submit">Entrar</button>        
        <h6><a href="Registro.jsp">Registrate en Hope</a></h6>
      </form>
		
	<footer>
		2015 &copy; HOPE
	</footer>		

    </div>
   </body>
     
</html>