<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

		<link rel="stylesheet" href="css/bootstrap.min.css">
		<link rel="stylesheet" href="css/Style_Index.css">

<title >HOPE</title>
</head>
<body>

<nav class="navbar navbar-inverse">
			<div class="container-fluid">
				<div class="navbar-header">
					<a class="navbar-brand" href="#">HOPE</a>
				</div>
		
				<div class ="pull-right">
					<ul class="nav navbar-nav" >
						
						<li><a href="Login.jsp">Login</a></li> 					 
						<li><a href="Registro.jsp">Registrate</a></li>
						
												
					</ul>
					
					
				</div>
				
				<div class ="pull-left">
					<ul class="nav navbar-nav" >
						<li><a href="Inicio"></a></li> 					 
						<li><a href="">Nosotros</a></li>
						<li><a href="Habitaciones.jsp">Habitaciones</a></li>
					</ul>
				</div>
			</div>
		</nav>
	
	
	
	
	
	
	
	
	<div id="carrousel-wrap">
		<div id="items-wrap">
			<c:forEach items="${Paises}" var="p">
				<div class="premiere-item">
					<p>${p.getNombre()}</p>
				</div>
			</c:forEach>
		</div>
	</div>
	
	
	<div class="jumbotron" style= "background-image:url('img\tikal.jpg')";>
      <div class="container">
        <h1>Elige tu preferencia</h1>
        <p>En mas de 20 paises, y mas de 500 opciones</p>
        <a href="#">Mas</a>
        
      </div>
    </div> 
    
    <div class="neighborhood-guides">
  <div class="container">
    <h2>HOPE</h2>
    <p>Elige tu preferencia</p>

   <div class="row">
  <div class="col-md-4">
    <div class="thumbnail">
      <img src="img/hot1.jpg">
    </div>
  </div>

  <div class="col-md-4">
   <div class="thumbnail">
      <img src="img/tikal.jpg" >
    </div>
    
  </div>

  <div class="col-md-4">
  	<div class="thumbnail">
      <img src="img/hab2.jpg" >
    </div>
    
  </div>

    <div class="learn-more">
	  <div class="container">
		<div class="row">
	      <div class="col-md-4">
			<h3>Habitaciones</h3>
			<p>Categorias y precios al alcance.</p>
			<p><a href="Habitaciones.jsp">Ve la habitaciones.</a></p>
	      </div>
		  <div class="col-md-4">
			<h3>Hoteles</h3>
			<p>Preferencia y calidad en mas de 500 opciones.</p>
			<p><a href="Hoteles.jsp">Ve los Hoteles</a></p>
		  </div>
		  <div class="col-md-4">
			<h3>Paises</h3>
			<p>Mas de 20 paises.</p>
			<p><a href="Paises.jsp">Ve los paises</a></p>
		  </div>
	    </div>
	  </div>
	 </div>
	
	
	
	<footer>
		2015 &copy; HOPE
	</footer>
</body>
</html>