package com.hope.database;

import java.sql.Connection;
import java.sql.Statement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.DriverManager;

/**
 * 
 * @author HOPE members
 *
 */

public class Database {
	
	private static final Database INSTANCE = new Database();
	private  Connection connection;	
	private Statement statement;
	
	
	/**
	 * Constructor 
	 */
	public Database(){
		
		try{
			
			Class.forName("com.mysql.jdbc.Driver");
			//connection = DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/hope_db", "administrator", "4dmin$");
			connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/hope_db", "root", "");
			statement = connection.createStatement();
			
		}catch(SQLException sqle){
			
			sqle.printStackTrace();
			
		}catch(ClassNotFoundException cnfe){
			
			cnfe.printStackTrace();
			
		}
	}
	
	/**
	 * 
	 * @param sentence
	 * @return
	 */
	public boolean runSentence(String sentence){
		
		boolean state = false;
		
		try{
			
			state = statement.execute(sentence);
			
		}catch(SQLException sqle){
			
			sqle.printStackTrace();
			
		}
		
		return state;
	}
	
	/**
	 * 
	 * @param query
	 * @return
	 */
	public ResultSet runQuery(String query){
		
		ResultSet data = null;
		
		try{
			
			data = statement.executeQuery(query);
			
		}catch(SQLException sqle){
			
			sqle.printStackTrace();
			return data;
			
		}
		
		return data;
	}
	
	/**
	 * 
	 * @return
	 */
	public static final Database getInstance(){
		return INSTANCE;
	}
}
