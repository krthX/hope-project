package com.hope.beans;

public class Ciudad {
	
	private int Id, IdDepartamento;
	private String Nombre;
	
	public Ciudad(){
		super();
	}
	
	public Ciudad(int id, String nombre, int iddepartamento){
		super();
		this.Id = id;
		this.Nombre = nombre;
		this.IdDepartamento = iddepartamento;
	}
	
	public void setId(int id){
		this.Id = id;
	}
	public int getId(){
		return Id;
	}
	
	public void setNombre(String nombre){
		this.Nombre = nombre;
	}
	public String getNombre(){
		return Nombre;
	}
	
	public void setIdDepartamento(int iddepartamento){
		this.IdDepartamento = iddepartamento;
	}
	public int getIdDepartamento(){
		return IdDepartamento;
	}
}
