package com.hope.beans;

public class Categoria_Habitacion {
	
	private int Id;
	private String Nombre, Descripcion;
	
	public Categoria_Habitacion(){
		super();
	}
	
	public Categoria_Habitacion(int id, String nombre, String descripcion){
		super();
		this.Id = id;
		this.Nombre = nombre;
		this.Descripcion = descripcion;
	}
	
	public void setId(int id){
		this.Id = id;
	}
	public int getId(){
		return Id;
	}
	
	public void setNombre(String nombre){
		this.Nombre = nombre;
	}
	public String getNombre(){
		return Nombre;
	}
	
	public void setDescripcion(String descripcion){
		this.Descripcion = descripcion;
	}
	public String getDescripcion(){
		return Descripcion;
	}
}
