package com.hope.beans;

import java.util.Date;

public class Facturacion {

	private int Id, IdReservacion;
	private double Total;
	private Date FechaHora;
	
	public Facturacion(){
		super();
	}
	
	public Facturacion(int id, int idreservacion, double total, Date fechahora){
		super();
		this.Id = id;
		this.IdReservacion = idreservacion;
		this.Total = total;
		this.FechaHora = fechahora;
	}
	
	public void setId(int id){
		this.Id = id;
	}
	public int getId(){
		return Id;
	}
	
	public void setIdReservacion(int idreservacion){
		this.IdReservacion = idreservacion;
	}
	public int getIdReservacion(){
		return IdReservacion;
	} 
	
	public void setTotal(double total){
		this.Total = total;
	}
	public double getTotal(){
		return Total;
	} 
	
	public void setFechaHora(Date fechahora){
		this.FechaHora = fechahora;
	}
	public Date getFechaHora(){
		return FechaHora;
	} 
}
