package com.hope.beans;

public class Habitacion {

	private int Id, PrecioNoche, IdEstado, IdHotel, IdCategoria_Habitacion;
	private String Nombre;
	
	public Habitacion(){
		super();
	}
	
	public Habitacion(int id,  int precionoche, int idestado, int idhotel,
			 int idcategoria_habitacion, String nombre){
		super();
		this.Id = id;
		this.Nombre = nombre;
		this.PrecioNoche = precionoche;
		this.IdEstado = idestado;
		this.IdHotel = idhotel;
		this.IdCategoria_Habitacion = idcategoria_habitacion;
	}
	
	public void setId(int id){
		this.Id = id;
	}
	public int getId(){
		return Id;
	}
	
	public void setNombre(String nombre){
		this.Nombre = nombre;
	}
	public String getNombre(){
		return Nombre;
	}
	
	public void setPrecioNoche(int precionoche){
		this.PrecioNoche = precionoche;
	}
	public int getPrecioNoche(){
		return PrecioNoche;
	} 
	
	public void setIdEstado(int idestado){
		this.IdEstado = idestado;
	}
	public int getIdEstado(){
		return IdEstado;
	} 
	
	public void setIdHotel(int idhotel){
		this.IdHotel = idhotel;
	}
	public int getIdHotel(){
		return IdHotel;
	} 
	
	public void setIdCategoria_Habitacion(int idcategoria_habitacion){
		this.IdCategoria_Habitacion = idcategoria_habitacion;
	}
	public int getIdCategoria_Habitacion(){
		return IdCategoria_Habitacion;
	} 
}
