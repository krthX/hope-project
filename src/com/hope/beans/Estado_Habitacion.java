package com.hope.beans;

public class Estado_Habitacion {

	private int Id;
	private String Nombre;
	
	public Estado_Habitacion(){
		super();
	}
	
	public Estado_Habitacion(int id, String nombre){
		super();
		this.Id = id;
		this.Nombre = nombre;
	}
	
	public void setId(int id){
		this.Id = id;
	}
	public int getId(){
		return Id;
	}
	
	public void setNombre(String nombre){
		this.Nombre = nombre;
	}
	public String getNombre(){
		return Nombre;
	}
}
