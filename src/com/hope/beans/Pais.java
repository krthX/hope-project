package com.hope.beans;

public class Pais {
	private int Id;
	private String Nombre;
	
	public Pais(int id, String nombre){
		super();
		this.Id = id;
		this.Nombre = nombre; 
	}
	
	public Pais(){
		super();
	}
	
	public void setId(int id){
		this.Id = id;
	}
	public int getId(){
		return Id;
	}
	
	public void setNombre( String nombre){
		this.Nombre = nombre;
	}
	public String getNombre(){
		return Nombre;
	}
}
