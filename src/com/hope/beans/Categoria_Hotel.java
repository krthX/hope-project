package com.hope.beans;

public class Categoria_Hotel {
	
	private int Id;
	private String Nombre, Descripcion;
	
	public Categoria_Hotel(){
		super();
	}
	
	public Categoria_Hotel(int id, String nombre, String descripcion){
		super();
		this.Id = id;
		this.Nombre = nombre;
		this.Descripcion = descripcion;
	}
	
	public void setId(int id){
		this.Id = id;
	}
	public int getId(){
		return Id;
	}
	
	public void setNombre(String nombre){
		this.Nombre = nombre;
	}
	public String getNombre(){
		return Nombre;
	}
	
	public void setDescripcion(String descripcion){
		this.Descripcion = descripcion;
	}
	public String getDescripcion(){
		return Descripcion;
	}
}
