package com.hope.beans;

public class Departamento {
	
	private int Id, IdPais;
	private String Nombre;
	
	public Departamento(){
		super();
	}
	
	public Departamento(int id, String nombre, int idPais){
		super();
		this.Id = id;
		this.Nombre = nombre;
		this.IdPais = idPais;
	}
	
	public void setId(int id){
		this.Id = id;
	}
	public int getId(){
		return Id;
	}
	
	public void setNombre(String nombre){
		this.Nombre = nombre;
	}
	public String getNombre(){
		return Nombre;
	}
	
	public void setIdPais(int idpais){
		this.IdPais = idpais;
	}
	public int getIdPais(){
		return IdPais;
	}
}
