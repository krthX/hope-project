package com.hope.beans;

import java.util.Date;

public class Reservacion {

	private int Id, IdHabitacion, IdCliente, CantidadNoches;
	private Date FechaHora;
	
	public Reservacion(){
		super();
	}
	
	public Reservacion(int id, int idhabitacion, int idcliente, int cantidadnoches, Date fechahora){
		super();
		this.Id =id;
		this.IdHabitacion = idhabitacion;
		this.IdCliente = idcliente;
		this.CantidadNoches = cantidadnoches;
		this.FechaHora = fechahora;
	}
	
	public void setId(int id){
		this.Id = id;
	}
	public int getId(){
		return Id;
	} 
	
	public void setIdHabitacion(int idhabitacion){
		this.IdHabitacion = idhabitacion;
	}
	public int getIdHabitacion(){
		return IdHabitacion;
	} 
	
	public void setIdCliente(int idcliente){
		this.IdCliente = idcliente;
	}
	public int getIdCliente(){
		return IdCliente;
	} 
	
	public void setCantidadNoches(int cantidadnoches){
		this.CantidadNoches = cantidadnoches;
	}
	public int getCantidadNoches(){
		return CantidadNoches;
	} 
	
	public void setFechaHora(Date fechahora){
		this.FechaHora = fechahora;
	}
	public Date getFechaHora(){
		return FechaHora;
	} 
}
