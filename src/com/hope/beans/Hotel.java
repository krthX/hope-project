package com.hope.beans;

public class Hotel {
	
	private int Id, Telefono, IdCategoria, IdCiudad;
	private String Nombre, Direccion, Correo, Latitud, Longitud;
	
	public Hotel(){
		super();
	}
	
	public Hotel(int id, String nombre, String direccion, int telefono, String correo,
			 String latitud, String longitud, int idcategoria, int idciudad){
		super();
		this.Id = id;
		this.Nombre = nombre;
		this.Direccion = direccion;
		this.Telefono = telefono;
		this.Correo = correo;
		this.Latitud = latitud;
		this.Longitud = longitud;
		this.IdCategoria = idcategoria;
		this.IdCiudad = idciudad;
	}
	
	public void setId(int id){
		this.Id = id;
	}
	public int getId(){
		return Id;
	}
	
	public void setNombre(String nombre){
		this.Nombre = nombre;
	}
	public String getNombre(){
		return Nombre;
	}
	
	public void setDireccion(String direccion){
		this.Direccion = direccion;
	}
	public String getDireccion(){
		return Direccion;
	}
	
	public void setTelefono(int telefono){
		this.Telefono = telefono;
	}
	public int getTelefono(){
		return Telefono;
	}
	
	public void setCorreo(String correo){
		this.Correo = correo;
	}
	public String getCorreo(){
		return Correo;
	}
	
	public void setLatitud(String latitud){
		this.Latitud = latitud;
	}
	public String getLatitud(){
		return Latitud;
	}
	
	public void setLongitud(String longitud){
		this.Longitud = longitud;
	}
	public String getLongitud(){
		return Longitud;
	}
	
	public void setIdCategoria(int idcategoria){
		this.IdCategoria = idcategoria;
	}
	public int getIdCategoria(){
		return IdCategoria;
	}
	
	public void setIdCiudad(int idciudad){
		this.IdCiudad = idciudad;
	}
	public int getIdCiudad(){
		return IdCiudad;
	}
}
