package com.hope.beans;

public class Usuario {
	
	private int Id, IdCliente;
	private String UserName, Password;
	
	public Usuario(){
		super();
	}
	
	public Usuario(int id, String username, String password, int idcliente){
		super();
		this.Id = id;
		this.UserName = username;
		this.Password = password;
		this.IdCliente = idcliente;
	}
	
	public void setId(int id){
		this.Id = id;
	}
	public int getId(){
		return Id;
	} 
	
	public void setNombre(String username){
		this.UserName = username;
	}
	public String getNombre(){
		return UserName;
	} 
	
	public void setPassword(String password){
		this.Password = password;
	}
	public String getPassword(){
		return Password;
	} 
	
	public void setIdCliente(int idcliente){
		this.IdCliente = idcliente;
	}
	public int getIdCliente(){
		return IdCliente;
	} 
}
