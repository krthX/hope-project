package com.hope.beans;

public class Cliente {
	
	private int Id, DPI, Telefono, NIT;
	private String Nombre, Apellido, NoTarjeta, Correo;
	
	public Cliente(){
		super();
	}
	
	public Cliente(int id, int dpi, String nombre, String apellido, String notarjeta, 
			int telefono, String correo, int nit){
		super();
		this.Id = id;
		this.DPI = dpi;
		this.Nombre = nombre;
		this.Apellido = apellido;
		this.NoTarjeta = notarjeta;
		this.Telefono = telefono;
		this.Correo = correo;
		this.NIT = nit;
	}
	
	public void setId(int id){
		this.Id = id;
	}
	public int getId(){
		return Id;
	} 
	
	public void setDPI(int dpi){
		this.DPI = dpi;
	}
	public int getDPI(){
		return DPI;
	} 
	
	public void setNombre(String nombre){
		this.Nombre = nombre;
	}
	public String getNombre(){
		return Nombre;
	} 
	
	public void setApellido(String apellido){
		this.Apellido = apellido;
	}
	public String getApellido(){
		return Apellido;
	} 
	
	public void setNoTarjeta(String notarjeta){
		this.NoTarjeta = notarjeta;
	}
	public String getNoTarjeta(){
		return NoTarjeta;
	} 
	
	public void setTelefono(int telefono){
		this.Telefono = telefono;
	}
	public int getTelefono(){
		return Telefono;
	} 
	
	public void setCorreo(String correo){
		this.Correo = correo;
	}
	public String getCorreo(){
		return Correo;
	} 
	
	public void setNIT(int nit){
		this.NIT = nit;
	}
	public int getNIT(){
		return NIT;
	} 
}
