package com.hope.handlers;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.sql.SQLException;

import com.hope.beans.Usuario;
import com.hope.database.Database;

public class Usuarios {

	private static final Usuarios INSTANCE = new Usuarios();
	
	public static final Usuarios getInstance () {
		return INSTANCE;
	}
	
	public Usuario getUsuarioId (Integer id) {
		Usuario usuario = null;
		
		ResultSet result = Database.getInstance().runQuery("SELECT * FROM usuario WHERE ID = " + id);
		
		try {
			while(result.next()){
				usuario = new Usuario(result.getInt("Id"), 
						result.getString("UserName"),
						result.getString("Password"),
						result.getInt("IdCliente"));
			}
		} catch (SQLException errSQL) {
			errSQL.printStackTrace();
		}
		
		return usuario;
	}
	
	public boolean eliminarUsuario(Integer id){
			
		try{
			
			Database.getInstance().runSentence("DELETE FROM usuario WHERE Id = " + id);
						
		}catch(Exception e){
			
			e.printStackTrace();
			return false;
		}
		
		return true;
			
	}
		
	public boolean actualizarUsuario(Usuario usuario){
			
		try{
				
			Database.getInstance().runSentence("UPDATE Usuario "
					+ "SET UserName = " + usuario.getNombre() 
					+ " Password = " + usuario.getPassword() 
					+ " IdCliente = " + usuario.getIdCliente()
					+" WHERE Id = "+usuario.getId());
			
		}catch(Exception e){
			
			e.printStackTrace();
			return false;
			
		}
		
		return true;
			
	}
	
	public boolean agregarUsuario(Usuario usuario){
		try{
				
			Database.getInstance().runSentence("INSERT INTO `hope_db`.`Usuario`"
					+ "(Id, UserName, Password, IdCliente) VALUES ("+null
					+" , " + usuario.getNombre() 
					+ " ," + usuario.getPassword() 
					+ " ,  " + usuario.getIdCliente()
					+")");
			
		}catch(Exception e){
			
			e.printStackTrace();
			return false;
			
		}
		
		return true;
	}
	public ArrayList<Usuario> getListaUsuarios(){
		
		ArrayList<Usuario> list = new ArrayList<Usuario>();
		
		ResultSet result = Database.getInstance().runQuery("SELECT * FROM Usuario");
		
		try {
			while(result.next()){
				list.add(new Usuario(result.getInt("Id"), 
						result.getString("UserName"),
						result.getString("Password"),
						result.getInt("IdCliente")));
			}
		} catch (SQLException errSQL) {
			errSQL.printStackTrace();
		}
		
		return list;
	}
	
}
