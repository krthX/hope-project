package com.hope.handlers;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.sql.SQLException;

import com.hope.beans.Ciudad;
import com.hope.database.Database;

public class Ciudades {

	private static final Ciudades INSTANCE = new Ciudades();
	
	public static final Ciudades getInstance () {
		return INSTANCE;
	}
	
	public Ciudad getCiudadId (Integer id) {
		Ciudad ciudad = null;
		
		ResultSet result = Database.getInstance().runQuery("SELECT * FROM Ciudad WHERE ID = " + id);
		
		try {
			while(result.next()){
				ciudad = new Ciudad(result.getInt("Id"),
						result.getString("Nombre"),
						result.getInt("idDepartamento"));
			}
		} catch (SQLException errSQL) {
			errSQL.printStackTrace();
		}
		
		return ciudad;
	}
	
	public boolean eliminarCiudad(Integer id){
			
		try{
			
			Database.getInstance().runSentence("DELETE FROM ciudad WHERE Id = " + id);
						
		}catch(Exception e){
			
			e.printStackTrace();
			return false;
		}
		
		return true;
			
	}
		
	public boolean actualizarCiudad(Ciudad ciudad){
			
		try{
				
			Database.getInstance().runSentence("UPDATE Ciudad "
					+ "SET Nombre = " + ciudad.getNombre() 
					+ " idDepartamento = " + ciudad.getIdDepartamento() 
					+" WHERE Id = "+ciudad.getId());
			
		}catch(Exception e){
			
			e.printStackTrace();
			return false;
			
		}
		
		return true;
			
	}
	
	public boolean agregarCiudad(Ciudad ciudad){
		try{
				
			Database.getInstance().runSentence("INSERT INTO `hope_db`.`ciudad`"
					+ "(Id, Nombre, idDepartamento) "
					+ "VALUES ("+null
					+"," + ciudad.getNombre() 
					+ " ," + ciudad.getIdDepartamento()
					+")");
			
		}catch(Exception e){
			
			e.printStackTrace();
			return false;
			
		}
		
		return true;
	}
	
	public ArrayList<Ciudad> getListaCiudades(){
		
		ArrayList<Ciudad> list = new ArrayList<Ciudad>();
		
		ResultSet result = Database.getInstance().runQuery("SELECT * FROM Ciudad");
		
		try {
			while(result.next()){
				list.add(new Ciudad(result.getInt("Id"),
						result.getString("Nombre"),
						result.getInt("idDepartamento")));
			}
		} catch (SQLException errSQL) {
			errSQL.printStackTrace();
		}
		
		return list;
	}
	
}
