package com.hope.handlers;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.sql.SQLException;

import com.hope.beans.Categoria_Habitacion;
import com.hope.database.Database;

public class Categoria_Habitaciones {

	private static final Categoria_Habitaciones INSTANCE = new Categoria_Habitaciones();
	
	public static final Categoria_Habitaciones getInstance () {
		return INSTANCE;
	}
	
	public Categoria_Habitacion getCategoriaHabitacionId (Integer id) {
		Categoria_Habitacion categoria = null;
		
		ResultSet result = Database.getInstance().runQuery("SELECT * FROM Categoria_Habitacion WHERE ID = " + id);
		
		try {
			while(result.next()){
				categoria = new Categoria_Habitacion(result.getInt("Id"),
						result.getString("Nombre"),
						result.getString("Descripcion"));
			}
		} catch (SQLException errSQL) {
			errSQL.printStackTrace();
		}
		
		return categoria;
	}
	
	public boolean eliminarCategoriaHabitacion(Integer id){
			
		try{
			
			Database.getInstance().runSentence("DELETE FROM Categoria_Habitacion WHERE Id = " + id);
						
		}catch(Exception e){
			
			e.printStackTrace();
			return false;
		}
		
		return true;
			
	}
		
	public boolean actualizarCategoriaHabitacion(Categoria_Habitacion categoria){
			
		try{
				
			Database.getInstance().runSentence("UPDATE Categoria_Habitacion "
					+ "SET Nombre = " + categoria.getNombre() 
					+ " Descripcion = " + categoria.getDescripcion() 
					+" WHERE Id = "+categoria.getId());
			
		}catch(Exception e){
			
			e.printStackTrace();
			return false;
			
		}
		
		return true;
			
	}
	
	public boolean AgregarCategoriaHabitacion(Categoria_Habitacion categoria){
		try{
				
			Database.getInstance().runSentence("INSERT INTO `hope_db`.`Categoria_Habitacion`"
					+ "(Id, Nombre, Descripcion) "
					+ "VALUES ("+null
					+"," + categoria.getNombre() 
					+ " ," + categoria.getDescripcion()
					+")");
			
		}catch(Exception e){
			
			e.printStackTrace();
			return false;
			
		}
		
		return true;
	}
	
	public ArrayList<Categoria_Habitacion> getListaCategoriasHabitacion(){
		
		ArrayList<Categoria_Habitacion> list = new ArrayList<Categoria_Habitacion>();
		
		ResultSet result = Database.getInstance().runQuery("SELECT * FROM Categoria_Habitacion");
		
		try {
			while(result.next()){
				list.add(new Categoria_Habitacion(result.getInt("Id"),
						result.getString("Nombre"),
						result.getString("Descripcion")));
			}
		} catch (SQLException errSQL) {
			errSQL.printStackTrace();
		}
		
		return list;
	}
	
}
