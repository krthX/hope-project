package com.hope.handlers;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.sql.SQLException;

import com.hope.beans.Facturacion;
import com.hope.database.Database;

public class Facturaciones {

	private static final Facturaciones INSTANCE = new Facturaciones();
	
	public static final Facturaciones getInstance () {
		return INSTANCE;
	}
	
	public Facturacion getFacturacionId (Integer id) {
		Facturacion factura = null;
		
		ResultSet result = Database.getInstance().runQuery("SELECT * FROM Facturacion "
				+ "WHERE ID = " + id);
		
		try {
			while(result.next()){
				factura = new Facturacion(result.getInt("Id"),
						result.getInt("idReservacion"),
						result.getDouble("Total"),
						result.getDate("FechaHora"));
			}
		} catch (SQLException errSQL) {
			errSQL.printStackTrace();
		}
		
		return factura;
	}
	
	public boolean eliminarFacturacion(Integer id){
			
		try{
			
			Database.getInstance().runSentence("DELETE FROM Facturacion WHERE Id = " + id);
						
		}catch(Exception e){
			
			e.printStackTrace();
			return false;
		}
		
		return true;
			
	}
		
	public boolean ActualizarFacturacion(Facturacion factura){
			
		try{
				
			Database.getInstance().runSentence("UPDATE Facturacion "
					+ "SET idReservacion = " + factura.getIdReservacion() 
					+ " Total = " + factura.getTotal()
					+" FechaHora= "+factura.getFechaHora()
					+" WHERE Id = "+factura.getId());
			
		}catch(Exception e){
			
			e.printStackTrace();
			return false;
			
		}
		
		return true;
			
	}
	
	public boolean agregarFactura(Facturacion factura){
		try{
				
				Database.getInstance().runSentence("INSERT INTO `hope_db`.`facturacion`"
						+ "(Id, idReservacion, Total, FechaHora) "
						+ "VALUES ("+null+"," 
						+ factura.getIdReservacion() 
						+ " ," + factura.getTotal()
						+" , "+factura.getFechaHora()+")");
				
			}catch(Exception e){
				
				e.printStackTrace();
				return false;
				
			}
			
			return true;
	}
	
	public ArrayList<Facturacion> getListaFacturacion(){
		
		ArrayList<Facturacion> list = new ArrayList<Facturacion>();
		
		ResultSet result = Database.getInstance().runQuery("SELECT * FROM Facturacion");
		
		try {
			while(result.next()){
				list.add(new Facturacion(result.getInt("Id"),
						result.getInt("idReservacion"),
						result.getDouble("Total"),
						result.getDate("FechaHora")));
			}
		} catch (SQLException errSQL) {
			errSQL.printStackTrace();
		}
		
		return list;
	}
	
}
