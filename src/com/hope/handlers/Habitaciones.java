package com.hope.handlers;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.sql.SQLException;

import com.hope.beans.Habitacion;
import com.hope.database.Database;

public class Habitaciones {

	private static final Habitaciones INSTANCE = new Habitaciones();
	
	public static final Habitaciones getInstance () {
		return INSTANCE;
	}
	
	public Habitacion getHabitacionId (Integer id) {
		Habitacion habitacion = null;
		
		ResultSet result = Database.getInstance().runQuery("SELECT * FROM habitacion WHERE ID = " + id);
		
		try {
			while(result.next()){
				habitacion = new Habitacion(result.getInt("Id"), 
						result.getInt("PrecionNoche"),
						result.getInt("idEstado"),
						result.getInt("idHotel"),
						result.getInt("idCategoriaH"),
						result.getString("Nombre"));
			}
		} catch (SQLException errSQL) {
			errSQL.printStackTrace();
		}
		
		return habitacion;
	}
	
	public boolean eliminarHabitacion(Integer id){
			
		try{
			
			Database.getInstance().runSentence("DELETE FROM habitacion WHERE Id = " + id);
						
		}catch(Exception e){
			
			e.printStackTrace();
			return false;
		}
		
		return true;
			
	}
		
	public boolean actualizarHabitacion(Habitacion habitacion){
			
		try{
				
			Database.getInstance().runSentence("UPDATE Habitacion "
					+ "SET Nombre = " + habitacion.getNombre() 
					+ " PrecioNoche = " + habitacion.getPrecioNoche() 
					+ " idEstado = " + habitacion.getIdEstado() 
					+ " idHotel = " + habitacion.getIdHotel() 
					+ " idCategoriaH = "+ habitacion.getIdCategoria_Habitacion() 
					+ " WHERE Id = " + habitacion.getId());
			
		}catch(Exception e){
			
			e.printStackTrace();
			return false;
			
		}
		
		return true;
			
	}
	public boolean agregarHabitacion(Habitacion habitacion){
		try{
				
			Database.getInstance().runSentence("INSERT INTO `hope_db`.`habitacion`"
					+ "(Id, Nombre, PrecioNoche,idEstado, idHotel, idCategoriaH)"
					+ " VALUES ("+null+"," + habitacion.getNombre() 
					+ " ," + habitacion.getPrecioNoche() 
					+ " ," + habitacion.getIdEstado() 
					+ " ," + habitacion.getIdHotel() 
					+ " ,"+ habitacion.getIdCategoria_Habitacion()
					+")");
			
		}catch(Exception e){
			
			e.printStackTrace();
			return false;
			
		}
		
		return true;
	}
	
	public ArrayList<Habitacion> getListaHabitaciones(){
		
		ArrayList<Habitacion> list = new ArrayList<Habitacion>();
		
		ResultSet result = Database.getInstance().runQuery("SELECT * FROM Habitacion");
		
		try {
			while(result.next()){
				list.add(new Habitacion(result.getInt("Id"), 
						result.getInt("PrecionNoche"),
						result.getInt("idEstado"),
						result.getInt("idHotel"),
						result.getInt("idCategoriaH"),
						result.getString("Nombre")));
			}
		} catch (SQLException errSQL) {
			errSQL.printStackTrace();
		}
		
		return list;
	}
	
}
