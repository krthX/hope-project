package com.hope.handlers;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.sql.SQLException;

import com.hope.beans.Departamento;
import com.hope.database.Database;

public class Departamentos {

	private static final Departamentos INSTANCE = new Departamentos();
	
	public static final Departamentos getInstance () {
		return INSTANCE;
	}
	
	public Departamento getDepartamentoId (Integer id) {
		Departamento departamento = null;
		
		ResultSet result = Database.getInstance().runQuery("SELECT * FROM departamento WHERE ID = " + id);
		
		try {
			while(result.next()){
				departamento = new Departamento(result.getInt("Id"),
						result.getString("Nombre"),
						result.getInt("idPais"));
			}
		} catch (SQLException errSQL) {
			errSQL.printStackTrace();
		}
		
		return departamento;
	}
	
	public boolean eliminarDepartamento(Integer id){
			
		try{
			
			Database.getInstance().runSentence("DELETE FROM departamento WHERE Id = " + id);
						
		}catch(Exception e){
			
			e.printStackTrace();
			return false;
		}
		
		return true;
			
	}
		
	public boolean actualizarDepartamento(Departamento departamento){
			
		try{
				
			Database.getInstance().runSentence("UPDATE Departamento "
					+ "SET Nombre = '" + departamento.getNombre() 
					+ "' idPais = " + departamento.getIdPais() 
					+" WHERE Id = "+departamento.getId());
			
		}catch(Exception e){
			
			e.printStackTrace();
			return false;
			
		}
		
		return true;
			
	}
	public boolean AgregarDepartamento(Departamento departamento){
		try{
				
			Database.getInstance().runSentence("INSERT INTO `hope_db`.`departamento`"
					+ "(Id, Nombre, idPais) "
					+ "VALUES ("+null
					+"," + departamento.getNombre() 
					+ " ," + departamento.getIdPais()
					+")");
			
		}catch(Exception e){
			
			e.printStackTrace();
			return false;
			
		}
		
		return true;
	}
	
	public ArrayList<Departamento> getListaDepartamentos(){
		
		ArrayList<Departamento> list = new ArrayList<Departamento>();
		
		ResultSet result = Database.getInstance().runQuery("SELECT * FROM Departamento");
		
		try {
			while(result.next()){
				list.add(new Departamento(result.getInt("Id"),
						result.getString("Nombre"),
						result.getInt("idPais")));
			}
		} catch (SQLException errSQL) {
			errSQL.printStackTrace();
		}
		
		return list;
	}
	
}
