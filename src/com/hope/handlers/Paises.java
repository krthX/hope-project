package com.hope.handlers;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.sql.SQLException;

import com.hope.beans.Pais;
import com.hope.database.Database;

public class Paises {

	private static final Paises INSTANCE = new Paises();
	
	public static final Paises getInstance () {
		return INSTANCE;
	}
	
	public Pais getPaisPorId (Integer id) {
		Pais pais = null;
		
		ResultSet result = Database.getInstance().runQuery("SELECT * FROM pais WHERE ID = " + id);
		
		try {
			while(result.next()){
				pais = new Pais(result.getInt("Id"), 
						result.getString("Nombre"));
			}
		} catch (SQLException errSQL) {
			errSQL.printStackTrace();
		}
		
		return pais;
	}
	
	public boolean eliminarPais(int id){
		
		try{
			
			Database.getInstance().runSentence("DELETE FROM pais WHERE Id = " + id);
						
		}catch(Exception e){
			
			e.printStackTrace();
			return false;
		}
		
		return true;
			
	}
		
	public boolean actualizarPais(Pais pais){
			
		try{
			
			Database.getInstance().runSentence("UPDATE pais "
					+ "SET Nombre = " + pais.getNombre() 
					+ " WHERE Id = " + pais.getId());
			
		}catch(Exception e){
			
			e.printStackTrace();
			return false;
			
		}
		
		return true;
		
	}
	
	public boolean agregarPais(Pais pais){
		try{
				
			Database.getInstance().runSentence("INSERT INTO `hope_db`.`pais` "
					+ "(`Id`, `Nombre`) "
					+ "VALUES ("+null
					+", '"+pais.getNombre()
					+" ')");
			
		}catch(Exception e){
			
			e.printStackTrace();
			return false;
			
		}
		
		return true;
	}
	
	public ArrayList<Pais> getListaPaises(){
		
		ArrayList<Pais> list = new ArrayList<Pais>();
		
		ResultSet result = Database.getInstance().runQuery("SELECT * FROM Pais");
		
		try {
			while(result.next()){
				list.add(new Pais(result.getInt("Id"), 
						result.getString("Nombre")));
			}
		} catch (SQLException errSQL) {
			errSQL.printStackTrace();
		}
		
		return list;
	}
	
}
