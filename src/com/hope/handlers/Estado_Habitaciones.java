package com.hope.handlers;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.sql.SQLException;

import com.hope.beans.Estado_Habitacion;
import com.hope.database.Database;

public class Estado_Habitaciones {

	private static final Estado_Habitaciones INSTANCE = new Estado_Habitaciones();
	
	public static final Estado_Habitaciones getInstance () {
		return INSTANCE;
	}
	
	public Estado_Habitacion getEstadoHId (Integer id) {
		Estado_Habitacion estado = null;
		
		ResultSet result = Database.getInstance().runQuery("SELECT * FROM Estado_Habitacion "
				+ "WHERE ID = " + id);
		
		try {
			while(result.next()){
				estado = new Estado_Habitacion(result.getInt("Id"),
						result.getString("Nombre"));
			}
		} catch (SQLException errSQL) {
			errSQL.printStackTrace();
		}
		
		return estado;
	}
	
	public boolean eliminarEstadoH(Integer id){
			
		try{
			
			Database.getInstance().runSentence("DELETE FROM Estado_Habitacion WHERE Id = " + id);
						
		}catch(Exception e){
			
			e.printStackTrace();
			return false;
		}
		
		return true;
			
	}
		
	public boolean actualizarEstadoH(Estado_Habitacion estado){
			
		try{
				
			Database.getInstance().runSentence("UPDATE Estado_Habitacion "
					+ "SET Nombre = " + estado.getNombre() 
					+" WHERE Id = "+estado.getId());
			
		}catch(Exception e){
			
			e.printStackTrace();
			return false;
			
		}
		
		return true;
			
	}
	
	public boolean agregarEstadoH(Estado_Habitacion estado){
		try{
				
				Database.getInstance().runSentence("INSERT INTO `hope_db`.`Estado_Habitacion`"
						+ "(Id, Nombre) "
						+ "VALUES ("+null
						+"," + estado.getNombre()
						+")");
				
			}catch(Exception e){
				
				e.printStackTrace();
				return false;
				
			}
			
			return true;
	}
	
	public ArrayList<Estado_Habitacion> getListaEstadosHabitacion(){
		
		ArrayList<Estado_Habitacion> list = new ArrayList<Estado_Habitacion>();
		
		ResultSet result = Database.getInstance().runQuery("SELECT * FROM Estado_Habitacion");
		
		try {
			while(result.next()){
				list.add(new Estado_Habitacion(result.getInt("Id"),
						result.getString("Nombre")));
			}
		} catch (SQLException errSQL) {
			errSQL.printStackTrace();
		}
		
		return list;
	}
	
}
