package com.hope.handlers;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.sql.SQLException;

import com.hope.beans.Reservacion;
import com.hope.database.Database;

public class Reservaciones {

	private static final Reservaciones INSTANCE = new Reservaciones();
	
	public static final Reservaciones getInstance () {
		return INSTANCE;
	}
	
	public Reservacion getReservacionId (Integer id) {
		Reservacion reser = null;
		
		ResultSet result = Database.getInstance().runQuery("SELECT * FROM Reservacion WHERE ID = " + id);
		
		try {
			while(result.next()){
				reser = new Reservacion(result.getInt("Id"),
						result.getInt("idHabitacion"),
						result.getInt("idCliente"),
						result.getInt("CantNoches"),
						result.getDate("FechaHora"));
			}
		} catch (SQLException errSQL) {
			errSQL.printStackTrace();
		}
		
		return reser;
	}
	
	public boolean eliminarReservacion(Integer id){
			
		try{
			
			Database.getInstance().runSentence("DELETE FROM Reservacion WHERE Id = " + id);
						
		}catch(Exception e){
			
			e.printStackTrace();
			return false;
		}
		
		return true;
			
	}
		
	public boolean actualizarReservacion(Reservacion reser){
			
		try{
				
			Database.getInstance().runSentence("UPDATE Reservacion "
					+ "SET idHabitacion = " + reser.getIdHabitacion() 
					+ " idCliente = " + reser.getIdCliente()
					+" CantNoches= "+reser.getCantidadNoches()
					+" FechaHora"+reser.getFechaHora() 
					+" WHERE Id = "+reser.getId());
			
		}catch(Exception e){
			
			e.printStackTrace();
			return false;
			
		}
		
		return true;
			
	}
	
	public boolean agregarReservacion(Reservacion reser){
		try{
				
			Database.getInstance().runSentence("INSERT INTO `hope_db`.`reservacion`"
					+ "(Id, idHabitacion, idCliente, CantNoches, FechaHora) "
					+ "VALUES ("+null
					+"," + reser.getIdHabitacion() 
					+ " ," + reser.getIdCliente()
					+" , "+reser.getCantidadNoches()
					+" FechaHora= "+reser.getFechaHora()+")");
			
		}catch(Exception e){
			
			e.printStackTrace();
			return false;
			
		}
		
		return true;
	}
	
	public ArrayList<Reservacion> getListaReservaciones(){
		
		ArrayList<Reservacion> list = new ArrayList<Reservacion>();
		
		ResultSet result = Database.getInstance().runQuery("SELECT * FROM Reservacion");
		
		try {
			while(result.next()){
				list.add(new Reservacion(result.getInt("Id"),
						result.getInt("idReservacion"),
						result.getInt("idCliente"),
						result.getInt("CantNoches"),
						result.getDate("FechaHora")));
			}
		} catch (SQLException errSQL) {
			errSQL.printStackTrace();
		}
		
		return list;
	}
	
}
