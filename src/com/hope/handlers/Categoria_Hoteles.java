package com.hope.handlers;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.sql.SQLException;

import com.hope.beans.Categoria_Hotel;
import com.hope.database.Database;

public class Categoria_Hoteles {

	private static final Categoria_Hoteles INSTANCE = new Categoria_Hoteles();
	
	public static final Categoria_Hoteles getInstance () {
		return INSTANCE;
	}
	
	public Categoria_Hotel getCategoriaHotelId (Integer id) {
		Categoria_Hotel categoria = null;
		
		ResultSet result = Database.getInstance().runQuery("SELECT * FROM Categoria_Hotel WHERE ID = " + id);
		
		try {
			while(result.next()){
				categoria = new Categoria_Hotel(result.getInt("Id"),
						result.getString("Nombre"),
						result.getString("Descripcion"));
			}
		} catch (SQLException errSQL) {
			errSQL.printStackTrace();
		}
		
		return categoria;
	}
	
	public boolean eliminarCategoriaHotel(Integer id){
			
		try{
			
			Database.getInstance().runSentence("DELETE FROM Categoria_Hotel WHERE Id = " + id);
						
		}catch(Exception e){
			
			e.printStackTrace();
			return false;
		}
		
		return true;
			
	}
		
	public boolean actualizarCategoriaHotel(Categoria_Hotel categoria){
			
		try{
				
			Database.getInstance().runSentence("UPDATE Categoria_Hotel "
					+ "SET Nombre = " + categoria.getNombre() 
					+ " Descripcion = " + categoria.getDescripcion() 
					+" WHERE Id = "+categoria.getId());
			
		}catch(Exception e){
			
			e.printStackTrace();
			return false;
			
		}
		
		return true;
			
	}
	
	public boolean agregarCategoriaHotel(Categoria_Hotel categoria){
		try{
				
			Database.getInstance().runSentence("INSERT INTO `hope_db`.`Categoria_Hotel`"
					+ "(Id, Nombre, Descripcion) "
					+ "VALUES ("+null
					+"," + categoria.getNombre() 
					+ " ," + categoria.getDescripcion()
					+")");
			
		}catch(Exception e){
			
			e.printStackTrace();
			return false;
			
		}
		
		return true;
	}
	
	public ArrayList<Categoria_Hotel> getListaCategoriasHotel(){
		
		ArrayList<Categoria_Hotel> list = new ArrayList<Categoria_Hotel>();
		
		ResultSet result = Database.getInstance().runQuery("SELECT * FROM Categoria_Hotel");
		
		try {
			while(result.next()){
				list.add(new Categoria_Hotel(result.getInt("Id"),
						result.getString("Nombre"),
						result.getString("Descripcion")));
			}
		} catch (SQLException errSQL) {
			errSQL.printStackTrace();
		}
		
		return list;
	}
	
}
