package com.hope.handlers;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.sql.SQLException;

import com.hope.beans.Hotel;
import com.hope.database.Database;
public class Hoteles {
	
	private static final Hoteles INSTANCE = new Hoteles();
	
	public static final Hoteles getInstance(){
		return INSTANCE;
	}
	
	public Hotel getHotelId(Integer id){
		Hotel hotel = null;
		ResultSet result = Database.getInstance().runQuery("SELECT * FROM Hotel WHERE Id = "+id);
		try {
			while(result.next()){
				hotel = new Hotel(result.getInt("Id"),
						result.getString("Nombre"),
						result.getString("Direccion"),
						result.getInt("Telefono"),
						result.getString("Correo"),
						result.getString("Latitud"),
						result.getString("Longitud"),
						result.getInt("idCategoria"),
						result.getInt("idCiudad"));
			}
		} catch (SQLException errSQL) {
			errSQL.printStackTrace();
		}
		
		return hotel;
	}
	
	public boolean eliminarHotel(Integer id){
		try{
			Database.getInstance().runSentence("DELETE FROM Hotel WHERE Id ="+id);
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}
		
		return true;
	}
	
	public boolean actualizarHotel(Hotel hotel){
		try{				
			Database.getInstance().runSentence("UPDATE Hotel "
					+ "SET Nombre = " + hotel.getNombre() 
					+ " Direccion = " + hotel.getDireccion()
					+" Telefono="+hotel.getTelefono()
					+" Correo="+hotel.getCorreo()
					+" Latitud="+hotel.getLatitud()
					+" Longitud="+hotel.getLongitud()
					+" idCategoria="+hotel.getIdCategoria()
					+" idCiudad="+hotel.getIdCiudad()
					+" WHERE Id = "+hotel.getId());			
		}catch(Exception e){			
			e.printStackTrace();
			return false;			
		}		
		return true;			
	}
	
	public boolean agregarHotel(Hotel hotel){
		try{			
			Database.getInstance().runSentence("INSERT INTO `hope_db`.`hotel`"
					+ "(Id, Nombre, Direccion, Telefono, Correo, Latitud, Longitud, idCategoria, idCiudad) "
					+ "VALUES ("+null
					+"," + hotel.getNombre() 
					+ " ," + hotel.getDireccion()
					+","+hotel.getTelefono()
					+","+hotel.getCorreo()
					+","+hotel.getLatitud()
					+","+hotel.getLongitud()
					+","+hotel.getIdCategoria()
					+","+hotel.getIdCiudad()+")");			
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	public ArrayList<Hotel> getListHoteles(){
		
		ArrayList<Hotel> list = new ArrayList<Hotel>();
		
		ResultSet result = Database.getInstance().runQuery("SELECT * FROM Hotel");
		
		try {
			while(result.next()){
				list.add(new Hotel(result.getInt("Id"),
						result.getString("Nombre"),
						result.getString("Direccion"),
						result.getInt("Telefono"),
						result.getString("Correo"),
						result.getString("Latitud"),
						result.getString("Longitud"),
						result.getInt("idCategoria"),
						result.getInt("idCiudad")));
			}
		} catch (SQLException errSQL) {
			errSQL.printStackTrace();
		}
		
		return list;
	}
}
