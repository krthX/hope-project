package com.hope.handlers;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.sql.SQLException;

import com.hope.beans.Cliente;
import com.hope.database.Database;

public class Clientes {

	private static final Clientes INSTANCE = new Clientes();
	
	public static final Clientes getInstance () {
		return INSTANCE;
	}
	
	public Cliente getClienteId (Integer id) {
		Cliente cliente = null;
		
		ResultSet result = Database.getInstance().runQuery("SELECT * FROM Cliente WHERE ID = " + id);
		
		try {
			while(result.next()){
				cliente = new Cliente(result.getInt("Id"),
						result.getInt("DPI"),
						result.getString("Nombre"),
						result.getString("Apellido"),
						result.getString("NoTarjeta"),
						result.getInt("Telefono"),
						result.getString("Correo"),
						result.getInt("NIT"));
			}
		} catch (SQLException errSQL) {
			errSQL.printStackTrace();
		}
		
		return cliente;
	}
	
	
	public boolean eliminarCliente(Integer id){
			
		try{
			
			Database.getInstance().runSentence("DELETE FROM Cliente WHERE Id = " + id);
						
		}catch(Exception e){
			
			e.printStackTrace();
			return false;
		}
		
		return true;
			
	}
		
	public boolean actualizarCliente(Cliente cliente){
			
		try{
				
			Database.getInstance().runSentence("UPDATE Cliente SET Nombre = " + cliente.getNombre() 
			+" DPI = " + cliente.getDPI()
			+" Apellido = " +cliente.getApellido()
			+" NoTarjeta= "+cliente.getNoTarjeta()
			+" Telefono = "+cliente.getTelefono()
			+" Correo= "+cliente.getCorreo()
			+" NIT= "+cliente.getNIT()
			+" WHERE Id = "+cliente.getId());
			
		}catch(Exception e){
			
			e.printStackTrace();
			return false;
			
		}
		
		return true;
			
	}
	
	public boolean agregarCliente(Cliente cliente){
		try{
				
				Database.getInstance().runSentence("INSERT INTO "
						+ "`hope_db`.`Cliente`"
						+ "(Id, DPI, Nombre, Apellido, NoTarjeta, Telefono, Correo, NIT)"
						+ " VALUES ("+null
						+"," + cliente.getDPI() 
						+ " ," + cliente.getNombre()
						+" , "+cliente.getApellido()
						+" , "+cliente.getNoTarjeta()
						+","+cliente.getTelefono()
						+" , "+cliente.getCorreo()
						+" , "+cliente.getNIT()+")");
				
			}catch(Exception e){
				
				e.printStackTrace();
				return false;
				
			}
			
			return true;
	}
	
	public ArrayList<Cliente> getListaClientes(){
		
		ArrayList<Cliente> list = new ArrayList<Cliente>();
		
		ResultSet result = Database.getInstance().runQuery("SELECT * FROM Cliente");
		
		try {
			while(result.next()){
				list.add(new Cliente(result.getInt("Id"),
						result.getInt("DPI"),
						result.getString("Nombre"),
						result.getString("Apellido"),
						result.getString("NoTarjeta"),
						result.getInt("Telefono"),
						result.getString("Correo"),
						result.getInt("NIT")));
			}
		} catch (SQLException errSQL) {
			errSQL.printStackTrace();
		}
		
		return list;
	}
	
}
